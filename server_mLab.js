require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT||3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./users.json');
const URL_mlab = 'https://api.mlab.com/api/1/databases/techu5db/collections/';
const apikey_malb =  'apiKey='+process.env.API_KEY_MLAB;

app.listen(port,function(){
console.log("Node 25 escuchando en el puerto : "+port);
    });

app.use(body_parser.json());
// operacion get  hola mundo
/*
app.get (URL_BASE+'users',
    function(request,response){
      console.log("Hola lima");
      response.send(usersFile);
    });

//peticion get a un unico usuario mediante un Id
/*
app.get(URL_BASE +'users/:id/:p1/:p2',
function(request, response){
  console.log('id: '+request.params.id);
  console.log('p1: '+request.params.p1);
  console.log('p2: '+request.params.p2);
  let pos = request.params.id - 1;
  response.send(usersFile[pos]);
});
*/

app.get(URL_BASE +'users3/:id',
function(req, res){
  console.log('id: '+request.params.id);
  let pos = request.params.id - 1;
  let respuesta = (usersFile[pos] == undefined)? {"msg":"no existente"} : usersFile[pos];
  let status_code =  (usersFile[pos] == undefined) ? 400:200;
  response.status(status_code).send(respuesta);
  });
//Operacion get con mlab
  app.get(URL_BASE +'users2',
  function(req, res){
    const http_client = request_json.createClient(URL_mlab);
    console.log("Cliente http creado correctamente ");
    let field_param = 'f={"_id":0}&'
    http_client.get('user?'+ field_param +apikey_malb,
    function(err, respuesta_Mlab, body){
      console.log('Error:'+err);
      console.log('respuesta_Mlab:'+respuesta_Mlab);
      console.log('Body:'+body);
      var response ={};
      if (err) {
        response = {"msg" : "error al recuperar users de mlab"}
        res.status(500);
      }else {
        if (body.length>0) {
          response=body;
        } else {
          response = {"msg" : "usuario no encontrado"}
          res.status(404);
        }
      }
      res.send(response);
    });
});
    //Operacion get con mlab
      app.get(URL_BASE +'user/:id/account',
      function(req, res){
        let id = req.params.id;
        const http_client = request_json.createClient(URL_mlab);
        console.log("Cliente http creado correctamente ");
        let field_param = 'f={"_id":0}&';
        let queryString = 'q={"user_id":'+id+'}&';
        http_client.get('account?'+ field_param+queryString +apikey_malb,
        function(err, respuesta_Mlab, body){
          console.log('Error:'+err);
          console.log('respuesta_Mlab:'+respuesta_Mlab);
          console.log('Body:'+body);
          var response ={};
          if (err) {
            response = {"msg" : "error al recuperar users de mlab"}
            res.status(500);
          }else {
            if (body.length>0) {
              response=body;
            } else {
              response = {"msg" : "usuario no encontrado"}
              res.status(404);
            }
          }
          res.send(response);
        });

  //  response.status(200).send({"msg": "Cliente HTTP a mlab creado correctamente "});
  });

  //Operacion get con mlab
    app.put(URL_BASE +'user/:id',
    function(req, res){
      let id = req.params.id;
      const http_client = request_json.createClient(URL_mlab);
      console.log("Cliente http creado correctamente ");
      let field_param = 'f={"_id":0}&';
      let queryString = 'q={"user_id":'+id+'}&';
      http_client.get('user?'+ field_param+queryString +apikey_malb,
      function(err, respuesta_Mlab, body){
        let cambio = '"{$set":'+JSON.stringify(body)+'}';
        console.log('Error:'+err);
        console.log('respuesta_Mlab:'+respuesta_Mlab);
        console.log('Body:'+body);
        var response ={};
        http_client.put('user?'+ field_param+queryString +apikey_malb,JSON.parse(cmabio),
          function(err, respuesta_Mlab, body){
        if (err) {
          response = {"msg" : "error al recuperar users de mlab"}
          res.status(500);
        }else {
          if (body.length>0) {
            response=body;
          } else {
            response = {"msg" : "usuario no encontrado"}
            res.status(404);
          }
        }
        res.send(response);
      });

//  response.status(200).send({"msg": "Cliente HTTP a mlab creado correctamente "});
});
});

//get user con id
app.get(URL_BASE +'users/:id',
function(req, res){
  console.log("get /aoi/2/user/id");
  console.log(req.params.id);
  let id = req.params.id;
  let queryString = 'q={"id_user":'+id+'}&';
  let httpClient = request_json.createClient(URL_mlab);

  httpClient.get('user?'+queryString+apikey_malb,
    function(error,res_lab,body){
      let response = {};
      if (error) {
        response = {"msg" : "error al recuperar users de mlab"};
        res.status(500);
      } else {
        if (body.length>0) {
          response = body;
        } else {
          response = {"msg" : "usuario no encontrado2"}
          res.status(404);
        }
      }
      res.send(response);
    });

});



app.post(URL_BASE +'users',
function(req, res){
  console.log('post a users');
  let tam = usersFile.length;
  let new_user = {
    "ID":tam+1,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
  }
  console.log(new_user);
  usersFile.push(new_user);
  res.send({"msg": " correctamente"});
});


app.put(URL_BASE + 'users2/:id',
   function(req, res){
     console.log("PUT /techu/v1/users/:id");
     let idBuscar = req.params.id;
     let updateUser = req.body;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id);
       if(usersFile[i].id == idBuscar) {
	     usersFile[i] = updateUser;
         res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
       }
     }
     res.send({"msg" : "Usuario no encontrado.", updateUser});
   });



/*
app.delete(
  usersFile.splice(pos,1)
);
*/
   // LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
         us.logged = true;
         writeUserDataToFile(usersFile);
         console.log("Login correcto!");
         response.send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
       } else {
         console.log("Login incorrecto.");
         response.send({"msg" : "Login incorrecto."});
       }
     }
   }
});
//conmlab
app.get(URL_BASE + 'users/:id',
  (request, response) => {
    console.log(request.params.id);
    let id = request.params.id;
    let queryString = `q={"id_user":${id}}&`;
    let fieldString = 'f={"_id":0}&';
    const httpClient = requestJson.createClient(URL_mlab);
    httpClient.get('users?' + queryString + fieldString +
    apikey_malb, (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = { 'msg': 'Error en la peticion a mLab' };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = { 'msg': 'Usuario no encontrado' };
        }
      }
      response.send(res);
    });
  });

  //logout
  app.post(URL_BASE + "logout",
    function (req, res) {
      console.log("POST /logout");
      var queryString = 'q={"email":"' + email + '","logged":true}&';
      var email = req.body.email;
      console.log(queryString);
      const clientemlab = request_json.createClient(URL_mlab);
      clientemlab.get('users?' + queryString + apikey_malb,
        function (error, respuestaMLab, body) {
          var respuesta = body[0];
          if (!error) {
            if (respuesta != undefined) {
              let logout = '{"$unset":{"logged":true}}';
              clientemlab.put('users?q={"id_user": ' + respuesta.id_user + '}&' + apikey_malb, JSON.parse(logout),
                function (err, res, body) {
                  res.send({ 'msg': 'Logout correcto', 'user': respuesta.email });
                });
            } else {
              res.status(404).send({ "msg": "Logout Error!" });
            }
          } else {
            res.status(500).send({ "msg": "Error en petición  a mLab." });
          }
        });
    });

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   });}
